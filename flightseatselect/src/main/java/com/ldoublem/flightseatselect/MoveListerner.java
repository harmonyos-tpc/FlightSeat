package com.ldoublem.flightseatselect;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.DragInfo;
import ohos.multimodalinput.event.ManipulationEvent;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public abstract class MoveListerner implements Component.TouchEventListener, Component.DraggedListener {
    public static final int MOVE_TO_LEFT = 0;
    public static final int MOVE_TO_RIGHT = MOVE_TO_LEFT + 1;
    public static final int MOVE_TO_UP = MOVE_TO_RIGHT + 1;
    public static final int MOVE_TO_DOWN = MOVE_TO_UP + 1;

    private static final int FLING_MIN_DISTANCE = 150;
    private static final int FLING_MIN_VELOCITY = 50;
    private boolean isScorllStart = false;
    private boolean isUpAndDown = false;
    GestureDetector mGestureDetector;
    float x1 = 0;
    float x2 = 0;
    float y1 = 0;
    float y2 = 0;

    public MoveListerner(AbilitySlice context) {
        super();
        mGestureDetector = new GestureDetector(context, menuGestureListener);
    }
    float startX = 0;
    float startY = 0;

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (mGestureDetector.onTouchEvent(event)) {
            if (TouchEvent.PRIMARY_POINT_DOWN == event.getAction()) {
                MmiPoint point = event.getPointerPosition(event.getIndex());
                startX = point.getX();
                startY = point.getY();
                return true;
            } else if (TouchEvent.PRIMARY_POINT_UP == event.getAction()) {
                MmiPoint point = event.getPointerPosition(event.getIndex());
                if (Math.abs(point.getX() - startX) < 5 &&
                        Math.abs(point.getY() - startY) < 5) {
                    Touch(point.getX(), point.getY());
                    return true;
                }
            }
        }
        // 处理手势结束
        switch (event.getAction() & TouchEvent.POINT_MOVE) {
            case TouchEvent.PRIMARY_POINT_UP:
                endGesture();
                break;
        }
        return false;
    }

    private void endGesture() {
        isScorllStart = false;
        isUpAndDown = false;
        moveOver();
    }

    public abstract void moveDirection(Component v, int direction, float distanceX, float distanceY);

    public abstract void moveUpAndDownDistance(ManipulationEvent event, int distance, int distanceY);

    public abstract void moveOver();

    public abstract void Touch(float x, float y);

    private GestureDetector.SimpleOnGestureListener menuGestureListener = new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
            float mOldY = e1.getPointerPosition(e1.getIndex()).getY();
            int y = (int) e2.getPointerPosition(e2.getIndex()).getY();
            if (!isScorllStart) {
                if (Math.abs(distanceX) / Math.abs(distanceY) > 2) {
                    // 左右滑动
                    isUpAndDown = false;
                    isScorllStart = true;
                } else if (Math.abs(distanceY) / Math.abs(distanceX) > 3) {
                    // 上下滑动
                    isUpAndDown = true;
                    isScorllStart = true;
                } else {
                    isScorllStart = false;
                }
            } else {
                // 算滑动速度的问题了
                if (isUpAndDown) {
                    // 是上下滑动，关闭左右检测
                    if (mOldY + 5 < y) {
                        moveUpAndDownDistance(e2, -3, (int) distanceY);
                    } else if (mOldY + 5 > y) {
                        moveUpAndDownDistance(e2, 3, (int) distanceY);
                    }
                }
            }
            return true;
        }

        @Override
        public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
            if (isUpAndDown)
                return false;
            if (e1.getPointerPosition(e1.getIndex()).getX() - e2.getPointerPosition(e2.getIndex()).getX() > FLING_MIN_DISTANCE
                    && Math.abs(velocityX) > FLING_MIN_VELOCITY) {
                // Fling left
                moveDirection(null, MOVE_TO_LEFT, e1.getPointerPosition(e1.getIndex()).getX() - e2.getPointerPosition(e2.getIndex()).getX(), e1.getPointerPosition(e1.getIndex()).getY() - e2.getPointerPosition(e2.getIndex()).getY());
            } else if (e2.getPointerPosition(e2.getIndex()).getX() - e1.getPointerPosition(e1.getIndex()).getX() > FLING_MIN_DISTANCE
                    && Math.abs(velocityX) > FLING_MIN_VELOCITY) {
                // Fling right
                moveDirection(null, MOVE_TO_RIGHT, e2.getPointerPosition(e2.getIndex()).getX() - e1.getPointerPosition(e1.getIndex()).getX(),
                        e2.getPointerPosition(e2.getIndex()).getY() - e1.getPointerPosition(e1.getIndex()).getY());
            } else if (e1.getPointerPosition(e1.getIndex()).getY() - e2.getPointerPosition(e2.getIndex()).getY() > FLING_MIN_DISTANCE
                    && Math.abs(velocityY) > FLING_MIN_VELOCITY) {
                // Fling up
                moveDirection(null, MOVE_TO_UP, 0, e1.getPointerPosition(e1.getIndex()).getY() - e2.getPointerPosition(e2.getIndex()).getY());
            } else {
                // Fling down
                moveDirection(null, MOVE_TO_DOWN, 0, e2.getPointerPosition(e2.getIndex()).getY() - e1.getPointerPosition(e1.getIndex()).getY());
            }
            return false;
        }
    };

    @Override
    public void onDragDown(Component component, DragInfo dragInfo) {}

    @Override
    public void onDragStart(Component component, DragInfo dragInfo) {}

    @Override
    public void onDragUpdate(Component component, DragInfo dragInfo) {}

    @Override
    public void onDragEnd(Component component, DragInfo dragInfo) {}

    @Override
    public void onDragCancel(Component component, DragInfo dragInfo) {}
}