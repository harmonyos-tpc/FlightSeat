/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ldoublem.flightseatselect;

import ohos.agp.components.VelocityDetector;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.multimodalinput.event.TouchEvent;

public class GestureDetector {
    private static final String TAG = "GestureDetector";

    public static final int FLAG_IS_GENERATED_GESTURE = 0x8;
    private static final int LONGPRESS_TIMEOUT = ViewConfiguration.getLongPressTimeout();
    private static final int TAP_TIMEOUT = ViewConfiguration.getTapTimeout();
    private static final int DOUBLE_TAP_TIMEOUT = ViewConfiguration.getDoubleTapTimeout();
    private static final int DOUBLE_TAP_MIN_TIME = ViewConfiguration.getDoubleTapMinTime();
    // constants for Message.what used by GestureHandler below
    private static final int SHOW_PRESS = 1;
    private static final int LONG_PRESS = 2;
    private static final int TAP = 3;
    private final GestureHandler mHandler;
    private final OnGestureListener mListener;
    private int mTouchSlopSquare;
    private int mDoubleTapTouchSlopSquare;
    private int mDoubleTapSlopSquare;
    private float mAmbiguousGestureMultiplier;
    private int mMinimumFlingVelocity;
    private int mMaximumFlingVelocity;
    private OnDoubleTapListener mDoubleTapListener;
    private OnContextClickListener mContextClickListener;
    private boolean mStillDown;
    private boolean mDeferConfirmSingleTap;
    private boolean mInLongPress;
    private boolean mInContextClick;
    private boolean mAlwaysInTapRegion;
    private boolean mAlwaysInBiggerTapRegion;
    private boolean mIgnoreNextUpEvent;
    // Whether a classification has been recorded by statsd for the current event stream. Reset on
    // ACTION_DOWN.
    private boolean mHasRecordedClassification;
    private TouchEvent mCurrentDownEvent;
    private TouchEvent mCurrentTouchEvent;
    private TouchEvent mPreviousUpEvent;
    private boolean mIsDoubleTapping;
    private float mLastFocusX;
    private float mLastFocusY;
    private float mDownFocusX;
    private float mDownFocusY;
    private boolean mIsLongpressEnabled;
    private VelocityDetector mVelocityTracker;

    @Deprecated
    public GestureDetector(OnGestureListener listener, EventHandler handler) {
        this(null, listener, handler);
    }

    @Deprecated
    public GestureDetector(OnGestureListener listener) {
        this(null, listener, null);
    }

    public GestureDetector(Context context, OnGestureListener listener) {
        this(context, listener, null);
    }

    public GestureDetector(Context context, OnGestureListener listener, EventHandler handler) {
        if (handler != null) {
            mHandler = new GestureHandler(handler);
        } else {
            mHandler = new GestureHandler(EventRunner.getMainEventRunner());
        }
        mListener = listener;
        if (listener instanceof OnDoubleTapListener) {
            setOnDoubleTapListener((OnDoubleTapListener) listener);
        }
        if (listener instanceof OnContextClickListener) {
            setContextClickListener((OnContextClickListener) listener);
        }
        init(context);
    }

    public GestureDetector(Context context, OnGestureListener listener, EventHandler handler,
                           boolean unused) {
        this(context, listener, handler);
    }

    private void init(Context context) {
        if (mListener == null) {
            LogUtil.error(TAG, "OnGestureListener must not be null");
        }
        mIsLongpressEnabled = true;

        // Fallback to support pre-donuts releases
        int touchSlop, doubleTapSlop, doubleTapTouchSlop;
        if (context == null) {
            //noinspection deprecation
            touchSlop = ViewConfiguration.getTouchSlop();
            doubleTapTouchSlop = touchSlop; // Hack rather than adding a hidden method for this
            doubleTapSlop = ViewConfiguration.getDoubleTapSlop();
            //noinspection deprecation
            mMinimumFlingVelocity = ViewConfiguration.getMinimumFlingVelocity();
            mMaximumFlingVelocity = ViewConfiguration.getMaximumFlingVelocity();
            mAmbiguousGestureMultiplier = ViewConfiguration.getAmbiguousGestureMultiplier();
        } else {
            touchSlop = ViewConfiguration.getScaledTouchSlop();
            doubleTapTouchSlop = ViewConfiguration.getScaledDoubleTapTouchSlop();
            doubleTapSlop = ViewConfiguration.getScaledDoubleTapSlop();
            mMinimumFlingVelocity = ViewConfiguration.getScaledMinimumFlingVelocity();
            mMaximumFlingVelocity = ViewConfiguration.getScaledMaximumFlingVelocity();
            mAmbiguousGestureMultiplier = ViewConfiguration.getScaledAmbiguousGestureMultiplier();
        }
        mTouchSlopSquare = touchSlop * touchSlop;
        mDoubleTapTouchSlopSquare = doubleTapTouchSlop * doubleTapTouchSlop;
        mDoubleTapSlopSquare = doubleTapSlop * doubleTapSlop;
    }

    public void setOnDoubleTapListener(OnDoubleTapListener onDoubleTapListener) {
        mDoubleTapListener = onDoubleTapListener;
    }

    public void setContextClickListener(OnContextClickListener onContextClickListener) {
        mContextClickListener = onContextClickListener;
    }

    public void setIsLongpressEnabled(boolean isLongpressEnabled) {
        mIsLongpressEnabled = isLongpressEnabled;
    }

    public boolean isLongpressEnabled() {
        return mIsLongpressEnabled;
    }

    public boolean onTouchEvent(TouchEvent ev) {
        final int action = ev.getAction();

        if (mCurrentTouchEvent != null) {
            mCurrentTouchEvent = null;
        }
        mCurrentTouchEvent = ev;

        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityDetector.obtainInstance();
        }
        mVelocityTracker.addEvent(ev);

        final boolean pointerUp = action == TouchEvent.PRIMARY_POINT_UP;
        final int skipIndex = pointerUp ? ev.getIndex() : -1;
        final boolean isGeneratedGesture = true;

        // Determine focal point
        float sumX = 0, sumY = 0;
        final int count = ev.getPointerCount();
        for (int i = 0; i < count; i++) {
            if (skipIndex == i) continue;
            sumX += ev.getPointerPosition(i).getX();
            sumY += ev.getPointerPosition(i).getY();
        }
        final int div = pointerUp ? count - 1 : count;
        final float focusX = sumX / div;
        final float focusY = sumY / div;
        boolean handled = false;

        switch (action) {
            case TouchEvent.OTHER_POINT_DOWN:
                mDownFocusX = mLastFocusX = focusX;
                mDownFocusY = mLastFocusY = focusY;
                // Cancel long press and taps
                cancelTaps();
                break;

            case TouchEvent.OTHER_POINT_UP:
                mDownFocusX = mLastFocusX = focusX;
                mDownFocusY = mLastFocusY = focusY;
                // Check the dot product of current velocities.
                // If the pointer that left was opposing another velocity vector, clear.
                mVelocityTracker.calculateCurrentVelocity(1000, mMaximumFlingVelocity, mMaximumFlingVelocity);
                final int upIndex = ev.getIndex();
                final int id1 = ev.getPointerId(upIndex);
                final float x1 = mVelocityTracker.getHorizontalVelocity();
                final float y1 = mVelocityTracker.getVerticalVelocity();
                for (int i = 0; i < count; i++) {
                    if (i == upIndex) continue;
                    final int id2 = ev.getPointerId(i);
                    final float x = x1 * mVelocityTracker.getHorizontalVelocity();
                    final float y = y1 * mVelocityTracker.getVerticalVelocity();
                    final float dot = x + y;
                    if (dot < 0) {
                        mVelocityTracker.clear();
                        break;
                    }
                }
                break;

            case TouchEvent.PRIMARY_POINT_DOWN:
                if (mDoubleTapListener != null) {
                    boolean hadTapMessage = mHandler.hasInnerEvent(TAP);
                    if (hadTapMessage) mHandler.removeEvent(TAP);
                    if ((mCurrentDownEvent != null) && (mPreviousUpEvent != null)
                            && hadTapMessage
                            && isConsideredDoubleTap(mCurrentDownEvent, mPreviousUpEvent, ev)) {
                        // This is a second tap
                        mIsDoubleTapping = true;
                        // Give a callback with the first tap of the double-tap
                        handled |= mDoubleTapListener.onDoubleTap(mCurrentDownEvent);
                        // Give a callback with down event of the double-tap
                        handled |= mDoubleTapListener.onDoubleTapEvent(ev);
                    } else {
                        // This is a first tap
                        mHandler.sendEvent(TAP, DOUBLE_TAP_TIMEOUT);
                    }
                }
                mDownFocusX = mLastFocusX = focusX;
                mDownFocusY = mLastFocusY = focusY;
                if (mCurrentDownEvent != null) {
                    mCurrentDownEvent = null;
                }
                mCurrentDownEvent = ev;
                mAlwaysInTapRegion = true;
                mAlwaysInBiggerTapRegion = true;
                mStillDown = true;
                mInLongPress = false;
                mDeferConfirmSingleTap = false;
                mHasRecordedClassification = false;

                if (mIsLongpressEnabled) {
                    mHandler.removeEvent(LONG_PRESS);
                    mHandler.sendTimingEvent(LONG_PRESS, mCurrentDownEvent.getStartTime() + ViewConfiguration.getLongPressTimeout());
                }
                mHandler.sendTimingEvent(SHOW_PRESS,
                        mCurrentDownEvent.getStartTime() + TAP_TIMEOUT);
                handled |= mListener.onDown(ev);
                break;

            case TouchEvent.POINT_MOVE:
                if (mInLongPress || mInContextClick) {
                    break;
                }
                final boolean hasPendingLongPress = mHandler.hasInnerEvent(LONG_PRESS);
                final float scrollX = mLastFocusX - focusX;
                final float scrollY = mLastFocusY - focusY;
                if (mIsDoubleTapping) {
                    // Give the move events of the double-tap
                    handled |= mDoubleTapListener.onDoubleTapEvent(ev);
                } else if (mAlwaysInTapRegion) {
                    final int deltaX = (int) (focusX - mDownFocusX);
                    final int deltaY = (int) (focusY - mDownFocusY);
                    int distance = (deltaX * deltaX) + (deltaY * deltaY);
                    int slopSquare = isGeneratedGesture ? 0 : mTouchSlopSquare;

                    final boolean shouldInhibitDefaultAction =
                            hasPendingLongPress;
                    if (shouldInhibitDefaultAction) {
                        // Inhibit default long press
                        if (distance > slopSquare) {
                            // The default action here is to remove long press. But if the touch
                            // slop below gets increased, and we never exceed the modified touch
                            // slop while still receiving AMBIGUOUS_GESTURE, we risk that *nothing*
                            // will happen in response to user input. To prevent this,
                            // reschedule long press with a modified timeout.
                            mHandler.removeEvent(LONG_PRESS);
                            final long longPressTimeout = ViewConfiguration.getLongPressTimeout();
                            mHandler.sendTimingEvent(LONG_PRESS, ev.getStartTime()
                                    + (long) (longPressTimeout * mAmbiguousGestureMultiplier));
                        }
                        // Inhibit default scroll. If a gesture is ambiguous, we prevent scroll
                        // until the gesture is resolved.
                        // However, for safety, simply increase the touch slop in case the
                        // classification is erroneous. Since the value is squared, multiply twice.
                        slopSquare *= mAmbiguousGestureMultiplier * mAmbiguousGestureMultiplier;
                    }
                    if (distance > slopSquare) {
                        handled = mListener.onScroll(mCurrentDownEvent, ev, scrollX, scrollY);
                        mLastFocusX = focusX;
                        mLastFocusY = focusY;
                        mAlwaysInTapRegion = false;
                        mHandler.removeEvent(TAP);
                        mHandler.removeEvent(SHOW_PRESS);
                        mHandler.removeEvent(LONG_PRESS);
                    }
                    int doubleTapSlopSquare = isGeneratedGesture ? 0 : mDoubleTapTouchSlopSquare;
                    if (distance > doubleTapSlopSquare) {
                        mAlwaysInBiggerTapRegion = false;
                    }
                } else if ((Math.abs(scrollX) >= 1) || (Math.abs(scrollY) >= 1)) {
                    handled = mListener.onScroll(mCurrentDownEvent, ev, scrollX, scrollY);
                    mLastFocusX = focusX;
                    mLastFocusY = focusY;
                }
                break;

            case TouchEvent.PRIMARY_POINT_UP:
                mStillDown = false;
                TouchEvent currentUpEvent = ev;
                if (mIsDoubleTapping) {
                    // Finally, give the up event of the double-tap
                    handled |= mDoubleTapListener.onDoubleTapEvent(ev);
                } else if (mInLongPress) {
                    mHandler.removeEvent(TAP);
                    mInLongPress = false;
                } else if (mAlwaysInTapRegion && !mIgnoreNextUpEvent) {
                    handled = mListener.onSingleTapUp(ev);
                    if (mDeferConfirmSingleTap && mDoubleTapListener != null) {
                        mDoubleTapListener.onSingleTapConfirmed(ev);
                    }
                } else if (!mIgnoreNextUpEvent) {
                    // A fling must travel the minimum tap distance
                    final VelocityDetector velocityTracker = mVelocityTracker;
                    final int pointerId = ev.getPointerId(0);
                    velocityTracker.calculateCurrentVelocity(1000, mMaximumFlingVelocity, mMaximumFlingVelocity);
                    final float velocityY = velocityTracker.getVerticalVelocity();
                    final float velocityX = velocityTracker.getVerticalVelocity();

                    if ((Math.abs(velocityY) > mMinimumFlingVelocity)
                            || (Math.abs(velocityX) > mMinimumFlingVelocity)) {
                        handled = mListener.onFling(mCurrentDownEvent, ev, velocityX, velocityY);
                    }
                }
                if (mPreviousUpEvent != null) {
                    mPreviousUpEvent = null;
                }
                // Hold the event we obtained above - listeners may have changed the original.
                mPreviousUpEvent = currentUpEvent;
                if (mVelocityTracker != null) {
                    // This may have been cleared when we called out to the
                    // application above.
                    mVelocityTracker.clear();
                    mVelocityTracker = null;
                }
                mIsDoubleTapping = false;
                mDeferConfirmSingleTap = false;
                mIgnoreNextUpEvent = false;
                mHandler.removeEvent(SHOW_PRESS);
                mHandler.removeEvent(LONG_PRESS);
                break;

            case TouchEvent.CANCEL:
                cancel();
                break;
        }
        handled = true;
        return handled;
    }

    private void cancel() {
        mHandler.removeEvent(SHOW_PRESS);
        mHandler.removeEvent(LONG_PRESS);
        mHandler.removeEvent(TAP);
        mVelocityTracker.clear();
        mVelocityTracker = null;
        mIsDoubleTapping = false;
        mStillDown = false;
        mAlwaysInTapRegion = false;
        mAlwaysInBiggerTapRegion = false;
        mDeferConfirmSingleTap = false;
        mInLongPress = false;
        mInContextClick = false;
        mIgnoreNextUpEvent = false;
    }

    private void cancelTaps() {
        mHandler.removeEvent(SHOW_PRESS);
        mHandler.removeEvent(LONG_PRESS);
        mHandler.removeEvent(TAP);
        mIsDoubleTapping = false;
        mAlwaysInTapRegion = false;
        mAlwaysInBiggerTapRegion = false;
        mDeferConfirmSingleTap = false;
        mInLongPress = false;
        mInContextClick = false;
        mIgnoreNextUpEvent = false;
    }

    private boolean isConsideredDoubleTap(TouchEvent firstDown, TouchEvent firstUp,
                                          TouchEvent secondDown) {
        if (!mAlwaysInBiggerTapRegion) {
            return false;
        }
        final long deltaTime = secondDown.getOccurredTime() - firstUp.getOccurredTime();
        if (deltaTime > DOUBLE_TAP_TIMEOUT || deltaTime < DOUBLE_TAP_MIN_TIME) {
            return false;
        }
        int deltaX = (int) firstDown.getPointerPosition(0).getX() - (int) secondDown.getPointerPosition(0).getX();
        int deltaY = (int) firstDown.getPointerPosition(0).getY() - (int) secondDown.getPointerPosition(0).getY();
        int slopSquare = true ? 0 : mDoubleTapSlopSquare;
        return (deltaX * deltaX + deltaY * deltaY < slopSquare);
    }

    private void dispatchLongPress() {
        mHandler.removeEvent(TAP);
        mDeferConfirmSingleTap = false;
        mInLongPress = true;
        mListener.onLongPress(mCurrentDownEvent);
    }

    public interface OnGestureListener {
        boolean onDown(TouchEvent e);
        void onShowPress(TouchEvent e);
        boolean onSingleTapUp(TouchEvent e);
        boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY);
        void onLongPress(TouchEvent e);
        boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY);
    }

    public interface OnDoubleTapListener {
        boolean onSingleTapConfirmed(TouchEvent e);
        boolean onDoubleTap(TouchEvent e);
        boolean onDoubleTapEvent(TouchEvent e);
    }

    public interface OnContextClickListener {
        boolean onContextClick(TouchEvent e);
    }

    public static class SimpleOnGestureListener implements OnGestureListener, OnDoubleTapListener,
            OnContextClickListener {
        public boolean onSingleTapUp(TouchEvent e) {
            return false;
        }

        public void onLongPress(TouchEvent e) {}

        public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
            return false;
        }
        public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
            return false;
        }

        public void onShowPress(TouchEvent e) {}

        public boolean onDown(TouchEvent e) {
            return false;
        }

        public boolean onDoubleTap(TouchEvent e) {
            return false;
        }

        public boolean onDoubleTapEvent(TouchEvent e) {
            return false;
        }

        public boolean onSingleTapConfirmed(TouchEvent e) {
            return false;
        }

        public boolean onContextClick(TouchEvent e) {
            return false;
        }
    }

    public static class ViewConfiguration {
        public static final int DEFAULT_LONG_PRESS_TIMEOUT = 400;
        private static final int TAP_TIMEOUT = 100;
        private static final int DOUBLE_TAP_TIMEOUT = 300;
        private static final int DOUBLE_TAP_MIN_TIME = 40;
        private static final int TOUCH_SLOP = 8;
        private static final int DOUBLE_TAP_SLOP = 100;
        private static final int MINIMUM_FLING_VELOCITY = 50;
        private static final int MAXIMUM_FLING_VELOCITY = 8000;
        private static final float AMBIGUOUS_GESTURE_MULTIPLIER = 2f;
        private static int mMinimumFlingVelocity = MINIMUM_FLING_VELOCITY;
        private static int mMaximumFlingVelocity = MAXIMUM_FLING_VELOCITY;
        private static int mTouchSlop = TOUCH_SLOP;
        private static int DOUBLE_TAP_TOUCH_SLOP = TOUCH_SLOP;
        private static int mDoubleTapTouchSlop = DOUBLE_TAP_TOUCH_SLOP;
        private static int mDoubleTapSlop = DOUBLE_TAP_SLOP;
        private static float mAmbiguousGestureMultiplier = AMBIGUOUS_GESTURE_MULTIPLIER;

        public static int getLongPressTimeout() {
            return DEFAULT_LONG_PRESS_TIMEOUT;
        }

        public static int getTapTimeout() {
            return TAP_TIMEOUT;
        }

        public static int getDoubleTapTimeout() {
            return DOUBLE_TAP_TIMEOUT;
        }

        public static int getDoubleTapMinTime() {
            return DOUBLE_TAP_MIN_TIME;
        }

        public static int getTouchSlop() {
            return TOUCH_SLOP;
        }

        public static int getScaledTouchSlop() {
            return mTouchSlop;
        }

        public static int getScaledDoubleTapTouchSlop() {
            return mDoubleTapTouchSlop;
        }

        public static int getDoubleTapSlop() {
            return DOUBLE_TAP_SLOP;
        }

        public static int getScaledDoubleTapSlop() {
            return mDoubleTapSlop;
        }

        public static int getMinimumFlingVelocity() {
            return MINIMUM_FLING_VELOCITY;
        }

        public static int getScaledMinimumFlingVelocity() {
            return mMinimumFlingVelocity;
        }

        public static int getMaximumFlingVelocity() {
            return MAXIMUM_FLING_VELOCITY;
        }

        public static int getScaledMaximumFlingVelocity() {
            return mMaximumFlingVelocity;
        }

        public static float getAmbiguousGestureMultiplier() {
            return AMBIGUOUS_GESTURE_MULTIPLIER;
        }

        public static float getScaledAmbiguousGestureMultiplier() {
            return mAmbiguousGestureMultiplier;
        }
    }

    private class GestureHandler extends EventHandler {
        public GestureHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        public GestureHandler(EventHandler handler) {
            super(handler.getEventRunner());
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            switch (event.eventId) {
                case SHOW_PRESS:
                    mListener.onShowPress(mCurrentDownEvent);
                    break;

                case LONG_PRESS:
                    dispatchLongPress();
                    break;

                case TAP:
                    // If the user's finger is still down, do not count it as a tap
                    if (mDoubleTapListener != null) {
                        if (!mStillDown) {
                            mDoubleTapListener.onSingleTapConfirmed(mCurrentDownEvent);
                        } else {
                            mDeferConfirmSingleTap = true;
                        }
                    }
                    break;

                default:
                    throw new RuntimeException("Unknown message "); //never
            }
        }
    }
}
