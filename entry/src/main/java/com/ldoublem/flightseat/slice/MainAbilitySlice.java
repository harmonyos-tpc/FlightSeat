/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ldoublem.flightseat.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

import com.ldoublem.flightseat.ResourceTable;
import com.ldoublem.flightseatselect.FlightSeatView;

/**
 * MainAbilitySlice extends AbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private FlightSeatView mFlightSeatView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mFlightSeatView = (FlightSeatView) findComponentById(ResourceTable.Id_fsv);
        mFlightSeatView.setMaxSelectStates(10);

        Button clearBtn = (Button) findComponentById(ResourceTable.Id_btn_clear);
        clearBtn.setClickedListener(this);

        Button zoomBtn = (Button) findComponentById(ResourceTable.Id_btn_zoom);
        zoomBtn.setClickedListener(this);

        Button gotoBtn = (Button) findComponentById(ResourceTable.Id_btn_goto);
        gotoBtn.setClickedListener(this);

        setTestData();
    }

    @Override
    public void onActive() {
        super.onActive();
        mFlightSeatView.setTouchListener(MainAbilitySlice.this);
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_clear:
                clear();
                break;
            case ResourceTable.Id_btn_zoom:
                zoom();
                break;
            case ResourceTable.Id_btn_goto:
                gotoPosition();
                break;
        }
    }

    public void zoom() {
        mFlightSeatView.startAnim(true);
    }

    public void gotoPosition() {
        mFlightSeatView.goCabinPosition(FlightSeatView.CabinPosition.Middle);
    }

    public void clear() {
        mFlightSeatView.setEmptySelecting();
    }

    private void setTestData() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 9; j = j + 2) {
                mFlightSeatView.setSeatSelected(j, i);
            }
        }
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 8; j = j + 2) {
                mFlightSeatView.setSeatSelected(i + 20, j);
            }
        }
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 8; j = j + 3) {
                mFlightSeatView.setSeatSelected(i + 35, j);
            }
        }
        for (int i = 11; i < 20; i++) {
            for (int j = 0; j < 8; j = j + 4) {
                mFlightSeatView.setSeatSelected(i + 35, j);
            }
        }
        mFlightSeatView.invalidate();
    }
}
