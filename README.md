# Introduction
FlightSeat: FlightSeat is a mobile library that helps for flight ticket seat selection purpose.

# Usage Instructions
Declare the flight seat view in layout as shown below  :
```
     <com.ldoublem.flightseatselect.FlightSeatView
        ohos:id="$+id:fsv"
        ohos:height="match_parent"
        ohos:width="match_parent"/>
```
FlightSeatView API :
```
 mFlightSeatView.startAnim(true);// false zoom in,true zoom out
 mFlightSeatView.setEmptySelecting();//clear
 mFlightSeatView.setSeatSelected(row, column);
 mFlightSeatView.goCabinPosition(FlightSeatView.CabinPosition.Middle);//top middle last
 mFlightSeatView.setMaxSelectStates(10);// Max seat selection limit

```

# Installation tutorial

1) For using FlightSeat module in sample app, modify entry build.gradle as below :

        dependencies {
        implementation project(':flightseatselect')
        }

2) For using FlightSeat in separate application make sure to add flightseatselect.har in entry/libs folder and modify build.gradle as below to add dependencies :

        dependencies {
        implementation fileTree(dir: 'libs', include: [ '*.jar', ' *.har'])
        }

# License

```
The MIT License (MIT)

Copyright (c) 2016 ldoublem

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

```